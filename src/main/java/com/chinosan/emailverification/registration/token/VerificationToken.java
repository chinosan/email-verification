package com.chinosan.emailverification.registration.token;

import com.chinosan.emailverification.user.User;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class VerificationToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private  String token;
    private Date expirationDate;
    @OneToOne
    @JoinColumn(name="user_id")
    private User user;
    private static final int EXPIRATION_TIME=15;

    public VerificationToken(String token, User user){
        super();
        this.token = token;
        this.user = user;
        this.expirationDate = this.getTokenExpirationTime();
    }
    public VerificationToken(String token){
        super();
        this.token = token;
        this.expirationDate = this.getTokenExpirationTime();
    }

    public Date getTokenExpirationTime(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(new Date().getTime());
        calendar.add(Calendar.MINUTE, EXPIRATION_TIME);

        return new Date(calendar.getTime().getTime());
    }

}
